const localStorage = window.localStorage;
export default {
	set(key: string, value: string) {
		localStorage.setItem(key, JSON.stringify(value));
	},
	get(key: string) {
		try {
			const value = localStorage.getItem(key);
			if (value === null || value === undefined || value === "") {
				return null;
			}
			return JSON.parse(value);
		} catch (err) {
			return null;
		}
	},
	remove(key: string) {
		localStorage.removeItem(key);
	}
};
