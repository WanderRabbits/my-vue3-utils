import axios from 'axios'
import { ElMessage } from 'element-plus'
import router from '../router'

axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? ' https://mobile-ms.uat.homecreditcfc.cn/mock/6217418b0b5aa1002717ed0b/vue2' : 'http://localhost'
axios.defaults.withCredentials = true
axios.defaults.headers.head['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.head['token'] = localStorage.getItem('token') || ''
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.response.use(res => {
  if (typeof res.data !== 'object') {
		ElMessage.error('服务端异常！')
    return Promise.reject(res)
  }
  if (res.data.code !== 200) {
    if (res.data.message) ElMessage.error(res.data.message)
    if (res.data.code === 403) {
      router.push({ path: '/login' })
    }
    if (res.data.data && window.location.hash === '/login') {
      axios.defaults.headers.head['token'] = res.data.data
    }
    return Promise.reject(res.data)
  }
  console.log(process.env.NODE_ENV)
  return res.data
})

export default axios